Project Overview
----------------

This project was developed with the Eclipse IDE, with Maven for dependency
resolution.

The project only contains a small handful of classes. The source code is in
the src/main/java/ directory and its subdirectories. The classes under the
application/ directory are DropWizard specific classes, classes under models/
are the persistent domain model classes, and classes under models/daos/ are
the data mapper objects.

Most of the logic is in the Filter class, under the filter/ directory.

The easiest way to start off is by importing the project from Eclipse as
"Import existing Maven project".

Building & Verification
-----------------------

Because of its nature as a proxy system without a User Interface, it is hard
to test its correctness. You could either try to use it as a proxy with your
browser (no instructions here), or run the tests, as specified below.

To build this project, use "mvn package". To run it, use
"java -jar target/webfilter-0.0.1.jar server settings.yml". You need to have
DerbyDB running first at localhost:1527, with credentials user:123.

Tests
-----

There are some simple tests to show that the Filter is showing the right
behaviour, while mocking the DAOs. These tests can be found in the
src/test/java/webfilter/filter/test/ directory. They can be run using the
JUnit runner.

The tests automatically get run by maven, and their results are stored under
target/surefire-reports/

Metrics
-------

In the project directory is included a script to bombard the proxy system with
requests. The script can take command-line arguments or use defaults. It
requires Node.js to run. After installing node.js, make sure you run

	npm install request

to install an important dependency of the script. The simplest way to execute
the script is:

	node endurance.js

For more details, please read the documentation at the top of the script.

The application is also set up to record metrics about proxy requests, and
write them every 15 seconds to a CSV file at

	metrics/webfilter.proxy.controller.ProxyFilter.response.time.csv
