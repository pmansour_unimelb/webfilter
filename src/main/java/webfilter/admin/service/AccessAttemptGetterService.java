package webfilter.admin.service;

import java.util.List;

import webfilter.proxy.dao.WebsiteAccessDao;
import webfilter.proxy.model.WebsiteAccess;

/**
 * Simple service to get all {@link WebsiteAccess} attempts recorded in the system.
 * 
 * @author peter
 */
public class AccessAttemptGetterService {

  private WebsiteAccessDao websiteAccessDao;

  public AccessAttemptGetterService(WebsiteAccessDao websiteAccessDao) {
    this.websiteAccessDao = websiteAccessDao;
  }

  /**
   * Get all {@link WebsiteAccess} attempts recorded in the system.
   */
  public List<WebsiteAccess> getAllWebsiteAccessAttempts() {
    return websiteAccessDao.findAll();
  }
}
