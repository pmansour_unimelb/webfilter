package webfilter.admin.service;

import java.util.Calendar;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import webfilter.admin.dao.RequestDAO;
import webfilter.admin.model.Request;
import webfilter.admin.model.Request.Status;
import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.dao.ExemptionDao;
import webfilter.proxy.model.BlockedWebsite;
import webfilter.proxy.model.Exemption;

import com.google.common.base.Optional;

/**
 * Service to get all unresolved requests This service can also resolve a request for a given
 * request id.
 * 
 * @author STEPHEN
 * 
 */
public class RequestResolverService {

  // Exemptions by default go for 365 days.
  private static final int DEFAULT_EXEMPTION_EXPIRY_NUMBER_OF_DAYS = 365;

  private BlockedWebsiteDao blockedWebsiteDao;
  private ExemptionDao exemptionDao;
  private RequestDAO requestDao;
  private WebsiteUnblockerService websiteUnblockerService;
  private SessionFactory sessionFactory;

  /**
   * Builds this service to use the given DAOs.
   */
  public RequestResolverService(SessionFactory sessionFactory, BlockedWebsiteDao blockedWebsiteDao,
      ExemptionDao exemptionDao, RequestDAO requestDao,
      WebsiteUnblockerService websiteUnblockerService) {
    this.blockedWebsiteDao = blockedWebsiteDao;
    this.exemptionDao = exemptionDao;
    this.requestDao = requestDao;
    this.websiteUnblockerService = websiteUnblockerService;
    this.sessionFactory = sessionFactory;
  }

  /**
   * Get a request using its given ID.
   */
  public Optional<Request> getById(Long requestId) {
    try {
      return requestDao.getById(requestId);
    } catch (HibernateException e) {
      return Optional.absent();
    }
  }

  /**
   * Resolve the request with the given ID to the given status.
   * 
   * @param requestId The ID of the request to resolve.
   * @param status The new status of the request.
   * @param reason The reason for an exemption, if needed.
   */
  public boolean resolve(Long requestId, Status status, String reason) {
    Session session = sessionFactory.openSession();
    Transaction tx = null;
    try {
      tx = session.beginTransaction();

      // First, get the request object in question.
      Optional<Request> request = requestDao.getById(session, requestId);

      if (!request.isPresent()) {
        throw new IllegalArgumentException("Invalid request ID.");
      }

      // First, perform any special activities for the new status (other than updating the status).
      switch (status) {
        case Exempted:
          // Ensure the blocked website still exists.
          if (!blockedWebsiteDao.findByHostname(session, request.get().getHostname()).isPresent()) {
            throw new IllegalArgumentException("No blocked website exists with this hostname.");
          }

          // Add an exemption.
          Calendar expiryDate = Calendar.getInstance();
          expiryDate.add(Calendar.DAY_OF_MONTH, DEFAULT_EXEMPTION_EXPIRY_NUMBER_OF_DAYS);

          exemptionDao.add(session, new Exemption(request.get().getUserIp(), request.get()
              .getHostname(), reason, Optional.of(expiryDate)));
          break;
        case Unblocked:
          // Get the website with the requested hostname.
          Optional<BlockedWebsite> website =
              blockedWebsiteDao.findByHostname(session, request.get().getHostname());

          if (!website.isPresent()) {
            throw new IllegalArgumentException("No blocked website exists with this hostname.");
          }

          // Unblock the given website.
          websiteUnblockerService.unblock(session, website.get().getId());
          break;
        default:
          break;
      }

      // Finally, update the request status
      if (requestDao.updateRequestStatus(session, requestId, status) == 0) {
        throw new HibernateException("Could not update request status.");
      }

      tx.commit();
      return true;
    } catch (Exception e) {
      if (tx != null && tx.isActive()) {
        tx.rollback();
      }
      return false;
    } finally {
      session.close();
    }
  }

  /**
   * Gets all unresolved requests
   * 
   * @return A list of {@link Request} objects
   */
  public List<Request> getUnresolvedRequests() {
    return this.requestDao.getUnresolvedRequests();
  }

}
