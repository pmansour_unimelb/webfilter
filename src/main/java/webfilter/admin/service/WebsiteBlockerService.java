package webfilter.admin.service;

import org.hibernate.HibernateException;

import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.model.BlockedWebsite;

/**
 * Service to add websites to the system blacklist.
 * 
 * @author peter
 */
public class WebsiteBlockerService {

  private BlockedWebsiteDao blockedWebsiteDao;

  public WebsiteBlockerService(BlockedWebsiteDao blockedWebsiteDao) {
    this.blockedWebsiteDao = blockedWebsiteDao;
  }

  /**
   * Add the website with the given hostname to the system blacklist, for the given reason.
   * 
   * @param hostname The hostname of the website to block.
   * @param reason The reason for this block.
   * @return Whether or not the block was successful.
   */
  public boolean block(String hostname, String reason) {
    try {
      // The block was successful if the newly-added blocked website got an ID.
      return blockedWebsiteDao.add(new BlockedWebsite(hostname, reason)).isPresent();
    } catch (HibernateException e) {
      e.printStackTrace(System.err);
      return false;
    }
  }

}
