package webfilter.admin.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.dao.ExemptionDao;
import webfilter.proxy.model.BlockedWebsite;

import com.google.common.base.Optional;

/**
 * A service to unblock a website with the given ID.
 * 
 * @author peter
 */
public class WebsiteUnblockerService {

  public static final String ERR_NOT_IN_BLACKLIST = "The given website ID is not in the blacklist.";

  private SessionFactory factory;
  private BlockedWebsiteDao blockedWebsiteDao;
  private ExemptionDao exemptionDao;

  public WebsiteUnblockerService(SessionFactory factory, BlockedWebsiteDao blockedWebsiteDao,
      ExemptionDao exemptionDao) {
    this.factory = factory;
    this.blockedWebsiteDao = blockedWebsiteDao;
    this.exemptionDao = exemptionDao;
  }

  /**
   * Remove the {@link BlockedWebsite} corresponding to the given website ID from the system
   * blacklist. Make sure no exemptions remain.
   * 
   * @param session The session to use for this transaction.
   * @param websiteId The ID of the {@link BlockedWebsite} to remove.
   * @return Whether or not the unblock was successful.
   */
  public boolean unblock(Long websiteId) {
    // First, open a session and start a transaction.
    Session session = factory.openSession();
    Transaction tx = null;
    try {
      tx = session.beginTransaction();
      unblock(session, websiteId);
      tx.commit();

      return true;
    } catch (Exception e) {
      if (tx != null && tx.isActive()) {
        tx.rollback();
      }
      e.printStackTrace(System.err);
      return false;
    } finally {
      session.close();
    }
  }

  /**
   * Remove the {@link BlockedWebsite} corresponding to the given website ID from the system
   * blacklist. Make sure no exemptions remain.
   * 
   * @param session The session to use for this transaction.
   * @param websiteId The ID of the {@link BlockedWebsite} to remove.
   * @return Whether or not the unblock was successful.
   */
  public void unblock(Session session, Long websiteId) throws Exception {
    // Get the website corresponding to this ID.
    Optional<BlockedWebsite> website = blockedWebsiteDao.getById(websiteId, session);

    // Exit if the website isn't even in the blacklist.
    if (!website.isPresent()) {
      throw new IllegalArgumentException(ERR_NOT_IN_BLACKLIST);
    }

    // Remove any exemptions with that hostname.
    exemptionDao.deleteAllByHostname(website.get().getHostname(), session);

    // Finally, remove the blocked website itself.
    blockedWebsiteDao.delete(website.get(), session);
  }
}
