package webfilter.admin.service;

import java.util.List;

import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.model.BlockedWebsite;

/**
 * Service to get the entire system blacklist.
 * 
 * @author peter
 */
public class BlacklistGetterService {

  private BlockedWebsiteDao blockedWebsiteDao;

  /**
   * Build this service using the given DAO.
   * 
   * @param blockedWebsiteDao The DAO to access {@link BlockedWebsite} objects.
   */
  public BlacklistGetterService(BlockedWebsiteDao blockedWebsiteDao) {
    this.blockedWebsiteDao = blockedWebsiteDao;
  }

  /**
   * Get all of the websites in the system blacklist.
   */
  public List<BlockedWebsite> getBlockedWebsites() {
    return blockedWebsiteDao.findAll();
  }
}
