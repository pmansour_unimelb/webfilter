package webfilter.admin.service;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

import org.hibernate.HibernateException;

import webfilter.admin.dao.AdminDAO;
import webfilter.admin.model.Admin;

import com.google.common.base.Optional;

/**
 * A service to implement Basic HTTP authentication using the Admin DAO and data model.
 */
public class AuthenticatorService implements Authenticator<BasicCredentials, Admin> {

  private AdminDAO adminDao;

  public AuthenticatorService(AdminDAO adminDao) {
    this.adminDao = adminDao;
  }

  /**
   * Attempt to authenticate to the system with the given credentials. These are checked against the
   * {@link Admin} model.
   * 
   * @param credentials The credentials to use for logging in.
   * @return An {@link Admin} if the credentials match one, or Optional.absent otherwise.
   * @throws AuthenticationException
   */
  public Optional<Admin> authenticate(BasicCredentials credentials) throws AuthenticationException {
    Optional<Admin> admin = Optional.absent();

    // Try to find an admin by that username.
    try {
      admin = adminDao.getByUsername(credentials.getUsername());
    } catch (HibernateException e) {
      throw new AuthenticationException(e);
    }

    // Fail if there's no admin with that username.
    if (!admin.isPresent()) {
      return Optional.absent();
    }

    // Check the password.
    if (admin.get().comparePassword(credentials.getPassword())) {
      return admin;
    } else {
      return Optional.absent();
    }
  }


}
