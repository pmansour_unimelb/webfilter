package webfilter.admin.service;

import org.hibernate.HibernateException;

import webfilter.admin.dao.AdminDAO;
import webfilter.admin.dao.RequestDAO;
import webfilter.admin.model.Admin;
import webfilter.admin.model.Request;

/**
 * Simple service to create some dummy data for testing the admin panel.
 * 
 * @author peter
 */
public class DummyAdminDataCreatorService {

  private AdminDAO adminDao;
  private RequestDAO requestDao;

  /**
   * Build the service using the given DAOs.
   * 
   * @param adminDao The DAO to use for persisting {@link Admin} objects.
   * @param requestDao The DAO to use for persisting {@link Request} objects.
   */
  public DummyAdminDataCreatorService(AdminDAO adminDao, RequestDAO requestDao) {
    this.adminDao = adminDao;
    this.requestDao = requestDao;
  }

  /**
   * Create some dummy data to go into the DB.
   * 
   * @throws HibernateException
   */
  public void createDummyData() throws HibernateException {
    adminDao.create(new Admin("admin", "password", "Cornel Olujimi"));
    requestDao.create(new Request("127.0.0.1", "facebook.com", "I want to check my news feed!"));
  }
}
