package webfilter.admin.service;

import org.hibernate.HibernateException;

import webfilter.admin.dao.RequestDAO;
import webfilter.admin.model.Request;

import com.google.common.base.Optional;

/**
 * The service to make a request in the system for a hostname to be unblocked.
 * 
 * @author peter
 */
public class RequestMakerService {

  private RequestDAO requestDao;

  public RequestMakerService(RequestDAO requestDao) {
    this.requestDao = requestDao;
  }

  /**
   * Create an unblocking request with the given data.
   * 
   * @param userIp The IP address of the user that issued the request.
   * @param hostname The hostname of the website requested to be unblocked.
   * @param message The message to show to the admin when considering the request.
   * @return The ID of the newly created request, or Optional.Absent().
   * @throws HibernateException
   */
  public Optional<Long> createRequest(String userIp, String hostname, String message)
      throws HibernateException {
    return requestDao.create(new Request(userIp, hostname, message));
  }
}
