package webfilter.admin.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represents a request for a blacklisted website to be unblocked.
 */
@Entity
@Table(name = "Request")
public class Request {

  /**
   * The possible states that a request can be in.
   */
  public enum Status {
    Unresolved, Exempted, Unblocked, Ignored
  };

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private long id;
  @Column(name = "status", nullable = false)
  private Status status;
  @Column(name = "timestamp", nullable = false)
  private Calendar timestamp;
  @Column(name = "userIp", nullable = false)
  private String userIp;
  @Column(name = "hostname", nullable = false)
  private String hostname;
  @Column(name = "message", nullable = false)
  private String message;

  /**
   * Empty constructor, to qualify as a JavaBean.
   */
  public Request() {}

  /**
   * Shorthand constructor, for initialising the fields.
   * 
   * @param userIp The IP address of the user making the request.
   * @param hostname The page the user is trying to access.
   * @param message The message to send to the admin.
   */
  public Request(String userIp, String hostname, String message) {
    this.status = Status.Unresolved;
    this.timestamp = Calendar.getInstance();
    this.userIp = userIp;
    this.hostname = hostname;
    this.message = message;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public Calendar getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Calendar timestamp) {
    this.timestamp = timestamp;
  }

  public String getUserIp() {
    return userIp;
  }

  public void setUserIp(String userIp) {
    this.userIp = userIp;
  }

  public String getHostname() {
    return hostname;
  }

  public void setHostname(String hostname) {
    this.hostname = hostname;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Get a string representation of the date at which this access was attempted, in the format
   * dd/mm/yyyy.
   */
  public String getDateString() {
    return new SimpleDateFormat("dd/MM/yyyy").format(timestamp.getTime());
  }

  /**
   * Get a string representation of the time at which this access was attempted, in the format
   * hh:mm:ssAM/PM.
   */
  public String getTimeString() {
    return new SimpleDateFormat("KK:mm:ssa").format(timestamp.getTime());
  }

}
