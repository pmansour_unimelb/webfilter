package webfilter.admin.model;

import java.nio.charset.Charset;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.common.base.Objects;
import com.google.common.hash.Hashing;

/**
 * Represents a system administrator who manages the blacklist, requests, and exemptions.
 */
@Entity
@Table(name = "Admin")
public class Admin {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private long id;
  @Column(name = "username", unique = true, nullable = false)
  private String username;
  @Column(name = "passwordHash", nullable = false)
  private String passwordHash;
  @Column(name = "fullName", nullable = false)
  private String fullName;

  /**
   * Empty constructor, to qualify as a JavaBean.
   */
  public Admin() {}

  /**
   * Shorthand constructor, for initialising the fields.
   * 
   * @param username The admin's username.
   * @param plaintextPassword The admin's plaintext password, which will be hashed.
   * @param fullName The admin's full name.
   */
  public Admin(String username, String plaintextPassword, String fullName) {
    setUsername(username);
    setPassword(plaintextPassword);
    setFullName(fullName);
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  /**
   * Hash the given password and compare it with the stored password.
   * 
   * @param plaintextPassword The plaintext password to compare.
   * @return Whether or not the passwords are the same.
   */
  public boolean comparePassword(String plaintextPassword) {
    return passwordHash.equals(hashPassword(plaintextPassword));
  }

  /**
   * Hash the given password and store it in this Admin object.
   * 
   * @param plaintextPassword The plaintext password to hash and store.
   */
  public void setPassword(String plaintextPassword) {
    setPasswordHash(hashPassword(plaintextPassword));
  }

  /**
   * Generate a hash for the given plaintext password.
   * 
   * @param plaintextPassword the plaintext password to hash.
   * @return A SHA-512 hash of the given password.
   */
  private String hashPassword(String plaintextPassword) {
    return Hashing.sha512().hashString(plaintextPassword, Charset.defaultCharset()).toString();
  }

  public String getPasswordHash() {
    return passwordHash;
  }

  public void setPasswordHash(String passwordHash) {
    this.passwordHash = passwordHash;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("id", id).add("username", username)
        .add("passwordHash", passwordHash).toString();
  }

}
