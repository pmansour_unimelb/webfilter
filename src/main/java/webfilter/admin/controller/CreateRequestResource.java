package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.views.View;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.service.RequestMakerService;
import webfilter.admin.view.SuccessView;

import com.google.common.base.Optional;

/**
 * Endpoint to handle creating new requests.
 * 
 * @author peter
 */
@Path("/request/add")
@Produces(MediaType.TEXT_HTML)
public class CreateRequestResource {

  public static final String SUCCESS_MESSAGE = "Request created successfully.";

  private RequestMakerService requestMakingService;

  public CreateRequestResource(RequestMakerService requestMakingService) {
    this.requestMakingService = requestMakingService;
  }

  @POST
  @Consumes("application/x-www-form-urlencoded")
  @Produces(MediaType.TEXT_HTML)
  public View create(@Auth(required = false) Admin admin, @FormParam("userIp") String userIp,
      @FormParam("hostname") String hostname, @FormParam("message") String message) {
    requestMakingService.createRequest(userIp, hostname, message);
    return new SuccessView(Optional.fromNullable(admin), SUCCESS_MESSAGE);
  }
}
