package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.jersey.params.LongParam;
import io.dropwizard.views.View;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.model.Request;
import webfilter.admin.service.RequestResolverService;
import webfilter.admin.view.FailureView;
import webfilter.admin.view.ReviewRequestView;

import com.google.common.base.Optional;

/**
 * Controller to review a particular request by its ID.
 * 
 * @author peter
 */
@Path("/requests/{id}/review")
@Produces(MediaType.TEXT_HTML)
public class ReviewRequestResource {

  public static final String GETTING_BY_ID_FAILURE_MESSAGE =
      "Could not find a request with the given ID.";

  private RequestResolverService requestResolverService;

  public ReviewRequestResource(RequestResolverService requestResolverService) {
    this.requestResolverService = requestResolverService;
  }

  /**
   * Get a page to review the request with the given ID.
   * 
   * @param admin The logged-in admin.
   * @param id The ID of the request to get.
   * @return A {@link View} showing the request to review.
   */
  @GET
  @Produces(MediaType.TEXT_HTML)
  public View getRequestById(@Auth Admin admin, @PathParam("id") LongParam id) {
    Optional<Request> request = requestResolverService.getById(id.get());
    if (request.isPresent()) {
      return new ReviewRequestView(admin, request.get());
    } else {
      return new FailureView(Optional.of(admin), GETTING_BY_ID_FAILURE_MESSAGE);
    }
  }
}
