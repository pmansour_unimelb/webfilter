package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.views.View;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.service.BlacklistGetterService;
import webfilter.admin.view.ManageBlacklistView;
import webfilter.proxy.model.BlockedWebsite;

/**
 * Resource to view and delete websites from the blacklist.
 * 
 * @author peter
 */
@Path("/blacklist")
@Produces(MediaType.TEXT_HTML)
public class ManageBlacklistResource {

  private BlacklistGetterService blockedWebsiteService;

  public ManageBlacklistResource(BlacklistGetterService blockedWebsiteService) {
    this.blockedWebsiteService = blockedWebsiteService;
  }

  /**
   * Get the system blacklist as a {@link View}.
   * 
   * @param admin The logged-in {@link Admin}.
   * @return The system blacklist in a {@link View}.
   */
  @GET
  public View getBlacklist(@Auth Admin admin) {
    List<BlockedWebsite> blacklist = blockedWebsiteService.getBlockedWebsites();
    return new ManageBlacklistView(admin, blacklist);
  }

}
