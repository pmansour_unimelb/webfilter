package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.jersey.params.LongParam;
import io.dropwizard.views.View;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.service.WebsiteUnblockerService;
import webfilter.admin.view.FailureView;
import webfilter.admin.view.SuccessView;

import com.google.common.base.Optional;

@Path("/blacklist/unblock/{websiteId}")
@Produces(MediaType.TEXT_HTML)
public class UnblockWebsiteResource {

  public static final String SUCCESS_MESSAGE = "Website #%d unblocked successfully.";
  public static final String FAILURE_MESSAGE = "Website #%d could not be unblocked.";

  private WebsiteUnblockerService websiteUnblockerService;

  public UnblockWebsiteResource(WebsiteUnblockerService websiteUnblockerService) {
    this.websiteUnblockerService = websiteUnblockerService;
  }

  /**
   * Remove the website with the given ID from the blacklist.
   * 
   * @param admin The {@link Admin} logged-in and requesting this unblocking.
   * @param websiteId The ID of the website to unblock.
   * @return A {@link View} confirming the unblock.
   */
  @POST
  public View unblock(@Auth Admin admin, @PathParam("websiteId") LongParam websiteId) {
    if (websiteUnblockerService.unblock(websiteId.get())) {
      return new SuccessView(Optional.of(admin), String.format(SUCCESS_MESSAGE, websiteId.get()));
    } else {
      return new FailureView(Optional.of(admin), String.format(FAILURE_MESSAGE, websiteId.get()));
    }
  }
}
