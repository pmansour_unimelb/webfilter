package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.jersey.params.LongParam;
import io.dropwizard.views.View;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.model.Request;
import webfilter.admin.model.Request.Status;
import webfilter.admin.service.RequestResolverService;
import webfilter.admin.view.FailureView;
import webfilter.admin.view.ManageRequestView;
import webfilter.admin.view.SuccessView;

import com.google.common.base.Optional;

/**
 * Resource To view all unresolved requests
 * 
 * @author STEPHEN
 * 
 */
@Path("/requests")
@Produces(MediaType.TEXT_HTML)
public class ManageRequestResource {

  public static final String RESOLVING_SUCCESS_MESSAGE = "The request was resolved successfully.";
  public static final String RESOLVING_FAILURE_MESSAGE = "Could not resolve the request.";

  private RequestResolverService requestResolverService;

  public ManageRequestResource(RequestResolverService requestResolverService) {
    this.requestResolverService = requestResolverService;
  }

  /**
   * Gets the list of unresolvedRequests as a {@link View}
   * 
   * @param admin
   * @return the unresolved requests in a {@link View}
   */
  @GET
  @Produces(MediaType.TEXT_HTML)
  public View getUnresolvedRequests(@Auth Admin admin) {
    List<Request> requests = this.requestResolverService.getUnresolvedRequests();
    return new ManageRequestView(admin, requests);
  }

  @POST
  @Produces(MediaType.TEXT_HTML)
  public View resolveRequest(@Auth Admin admin, @FormParam("requestId") LongParam requestId,
      @FormParam("action") String action, @FormParam("reason") @DefaultValue("") String reason) {
    if (requestResolverService.resolve(requestId.get(), Status.valueOf(action), reason)) {
      return new SuccessView(Optional.of(admin), RESOLVING_SUCCESS_MESSAGE);
    } else {
      return new FailureView(Optional.of(admin), RESOLVING_FAILURE_MESSAGE);
    }
  }
}
