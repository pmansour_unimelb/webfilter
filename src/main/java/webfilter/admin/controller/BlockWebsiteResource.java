package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.views.View;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.service.WebsiteBlockerService;
import webfilter.admin.view.BlockWebsiteView;
import webfilter.admin.view.FailureView;
import webfilter.admin.view.SuccessView;

import com.google.common.base.Optional;

@Path("/blacklist/add")
@Produces(MediaType.TEXT_HTML)
public class BlockWebsiteResource {

  public static final String SUCCESS_MESSAGE = "Website [%s] blocked successfully!";
  public static final String FAILURE_MESSAGE = "Website [%s] could not be blocked.";

  private WebsiteBlockerService websiteBlockerService;

  public BlockWebsiteResource(WebsiteBlockerService websiteBlockerService) {
    this.websiteBlockerService = websiteBlockerService;
  }

  @GET
  public View get(@Auth Admin admin) {
    return new BlockWebsiteView(admin);
  }

  @POST
  @Consumes("application/x-www-form-urlencoded")
  @Produces(MediaType.TEXT_HTML)
  public View add(@Auth Admin admin, @FormParam("hostname") String hostname,
      @FormParam("reason") String reason) {
    if (websiteBlockerService.block(hostname, reason)) {
      return new SuccessView(Optional.of(admin), String.format(SUCCESS_MESSAGE, hostname));
    } else {
      return new FailureView(Optional.of(admin), String.format(FAILURE_MESSAGE, hostname));
    }
  }
}
