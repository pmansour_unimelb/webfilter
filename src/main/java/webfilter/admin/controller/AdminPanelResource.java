package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.views.View;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.view.AdminPanelView;

/**
 * Simple resource to gets the admin panel page.
 * 
 * @author peter
 */
@Path("/")
@Produces(MediaType.TEXT_HTML)
public class AdminPanelResource {

  @GET
  @Produces(MediaType.TEXT_HTML)
  public View get(@Auth Admin admin) {
    return new AdminPanelView(admin);
  }
}
