package webfilter.admin.controller;

import io.dropwizard.auth.Auth;
import io.dropwizard.views.View;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import webfilter.admin.model.Admin;
import webfilter.admin.service.AccessAttemptGetterService;
import webfilter.admin.view.AccessAttemptsView;
import webfilter.proxy.model.WebsiteAccess;

@Path("/accessAttempts")
@Produces(MediaType.TEXT_HTML)
public class AccessAttemptsResource {

  private AccessAttemptGetterService accessAttemptGetterService;

  public AccessAttemptsResource(AccessAttemptGetterService accessAttemptGetterService) {
    this.accessAttemptGetterService = accessAttemptGetterService;
  }

  /**
   * Get all of the {@link WebsiteAccess} attempts recorded in the system.
   * 
   * @param admin The {@link Admin} logged-in to the system.
   */
  @GET
  @Produces(MediaType.TEXT_HTML)
  public View get(@Auth Admin admin) {
    return new AccessAttemptsView(admin, accessAttemptGetterService.getAllWebsiteAccessAttempts());
  }
}
