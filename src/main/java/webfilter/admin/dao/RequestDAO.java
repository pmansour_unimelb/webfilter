package webfilter.admin.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import webfilter.admin.model.Request;
import webfilter.admin.model.Request.Status;

import com.google.common.base.Optional;

/**
 * Controls the persistence of {@link Request} objects.
 */
public class RequestDAO extends AbstractDAO<Request> {

  // The queries used by this DAO.
  private static final String Q_FIND_UNRESOLVED_REQUESTS =
      "from Request r where r.status = :status";
  private static final String Q_UPDATE_REQUEST_STATUS =
      "update Request r set r.status = :status where r.id = :id";
  private static final String Q_FLD_ID = "id";
  private static final String Q_FLD_STATUS = "status";

  private SessionFactory factory;

  public RequestDAO(SessionFactory factory) {
    super(factory);
    this.factory = factory;
  }

  /**
   * Persist the given {@link Request} object. Assumes a new object, as it will be given a new
   * identifier.
   * 
   * @param request The new {@link Request} to be persisted.
   * @return The ID of the newly-persisted object, or Optional.absent().
   * @throws HibernateException
   */
  public Optional<Long> create(Request request) throws HibernateException {
    // Perform this save in a new Session.
    Session session = factory.openSession();
    try {
      session.save(request);
      session.flush();

      return Optional.fromNullable(request.getId());
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Update the request with the given ID to the given status, using the given session.
   * 
   * @param session The session to use for the query. Assumed to be open and in a transaction.
   *        Neither the session nor the transaction are closed by this method.
   * @param requestId The ID of the request to update.
   * @param newStatus The new status to give it.
   * @return The number of records updated.
   */
  public Integer updateRequestStatus(Session session, Long requestId, Status newStatus) {
    return session.createQuery(Q_UPDATE_REQUEST_STATUS).setLong(Q_FLD_ID, requestId)
        .setParameter(Q_FLD_STATUS, newStatus).executeUpdate();
  }

  /**
   * Get a specific request by its ID.
   * 
   * @param id The ID of the request to load.
   * @return The request object if one was found, or Optional.absent() otherwise.
   */
  public Optional<Request> getById(Long id) {
    Session session = factory.openSession();
    try {
      return getById(session, id);
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Get a specific request by its ID.
   * 
   * @param session The session to use for this request.
   * @param id The ID of the request to load.
   * @return The request object if one was found, or Optional.absent() otherwise.
   */
  public Optional<Request> getById(Session session, Long id) {
    return Optional.fromNullable((Request) session.get(Request.class, id));
  }

  /**
   * Find all the requests in the DB that are still marked as unresolved.
   * 
   * @return All the matching requests.
   * @throws HibernateException
   */
  public List<Request> getUnresolvedRequests() throws HibernateException {
    Session session = factory.openSession();
    try {
      return list(session.createQuery(Q_FIND_UNRESOLVED_REQUESTS).setParameter(Q_FLD_STATUS,
          Status.Unresolved));
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }
}
