package webfilter.admin.dao;

import io.dropwizard.hibernate.AbstractDAO;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import webfilter.admin.model.Admin;

import com.google.common.base.Optional;

/**
 * Controls the persistence of {@link Admin} objects.
 */
public class AdminDAO extends AbstractDAO<Admin> {

  // The queries used by this DAO.
  public static final String Q_FIND_ADMIN_BY_USERNAME = "from Admin a where a.username = :username";
  public static final String Q_FLD_USERNAME = "username";

  private SessionFactory factory;

  public AdminDAO(SessionFactory factory) {
    super(factory);
    this.factory = factory;
  }

  /**
   * Create a new {@link Admin} in the DB from the given model.
   * 
   * @param admin The {@link Admin} to save to the DB.
   * @return The newly-added {@link Admin}'s ID, or Optional.Absent().
   * @throws HibernateException
   */
  public Optional<Long> create(Admin admin) throws HibernateException {
    Session session = factory.openSession();
    try {
      session.save(admin);
      session.flush();

      return Optional.fromNullable(admin.getId());
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Finds the {@link Admin} relating with a particular username.
   * 
   * @param username The username to look for.
   * @return the {@link Admin} object relating to a particular user, or {@link Optional.Absent}.
   * @throws HibernateException
   */
  public Optional<Admin> getByUsername(String username) throws HibernateException {
    Session session = factory.openSession();
    try {
      Optional<Admin> admin =
          Optional.fromNullable(uniqueResult(session.createQuery(Q_FIND_ADMIN_BY_USERNAME)
              .setString(Q_FLD_USERNAME, username)));
      return admin;
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }
}
