package webfilter.admin.view;

import io.dropwizard.views.View;
import webfilter.admin.model.Admin;

/**
 * Represents the Admin Panel's main page.
 */
public class AdminPanelView extends View {

  private static final String TEMPLATE_NAME = "AdminPanel.mustache";

  private Admin admin;

  public AdminPanelView(Admin admin) {
    super(TEMPLATE_NAME);

    this.admin = admin;
  }

  public Admin getAdmin() {
    return admin;
  }

}
