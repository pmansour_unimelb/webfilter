package webfilter.admin.view;

import io.dropwizard.views.View;
import webfilter.admin.model.Admin;

import com.google.common.base.Optional;

/**
 * Represents a general {@link View} to use whenever a failure screen needs to be shown.
 * 
 * @author peter
 */
public class FailureView extends View {

  private static final String TEMPLATE_NAME = "Failure.mustache";

  private Admin admin;
  private String message;

  /**
   * Create a new failure message view.
   * 
   * @param admin If present, show the admin header.
   * @param message The failure message to show.
   */
  public FailureView(Optional<Admin> admin, String message) {
    super(TEMPLATE_NAME);

    this.admin = admin.orNull();
    this.message = message;
  }

  public Admin getAdmin() {
    return admin;
  }

  public String getMessage() {
    return message;
  }
}
