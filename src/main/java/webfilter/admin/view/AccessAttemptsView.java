package webfilter.admin.view;

import io.dropwizard.views.View;

import java.util.List;

import webfilter.admin.model.Admin;
import webfilter.proxy.model.WebsiteAccess;

/**
 * Represents the view of the admin panel to show the access attempts.
 * 
 * @author peter
 */
public class AccessAttemptsView extends View {

  private static final String TEMPLATE_NAME = "AccessAttempts.mustache";

  private Admin admin;
  private List<WebsiteAccess> accessAttempts;

  public AccessAttemptsView(Admin admin, List<WebsiteAccess> accessAttempts) {
    super(TEMPLATE_NAME);

    this.admin = admin;
    this.accessAttempts = accessAttempts;
  }

  public Admin getAdmin() {
    return admin;
  }

  public List<WebsiteAccess> getAccessAttempts() {
    return accessAttempts;
  }

}
