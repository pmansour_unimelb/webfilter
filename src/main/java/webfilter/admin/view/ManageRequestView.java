package webfilter.admin.view;

import io.dropwizard.views.View;

import java.util.List;

import webfilter.admin.model.Admin;
import webfilter.admin.model.Request;

/**
 * Represents the view the admin has over all unresolved requests
 * 
 * @author STEPHEN
 * 
 */
public class ManageRequestView extends View {

  private static final String TEMPLATE_NAME = "ManageRequests.mustache";

  private Admin admin;
  private List<Request> requests;

  public ManageRequestView(Admin admin, List<Request> request) {
    super(TEMPLATE_NAME);
    this.admin = admin;
    this.requests = request;
  }

  public Admin getAdmin() {
    return admin;
  }

  public List<Request> getRequests() {
    return requests;
  }

}
