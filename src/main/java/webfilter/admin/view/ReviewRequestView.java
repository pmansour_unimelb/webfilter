package webfilter.admin.view;

import io.dropwizard.views.View;
import webfilter.admin.model.Admin;
import webfilter.admin.model.Request;

public class ReviewRequestView extends View {

  public static final String TEMPLATE_NAME = "ReviewRequest.mustache";

  private Admin admin;
  private Request request;

  public ReviewRequestView(Admin admin, Request request) {
    super(TEMPLATE_NAME);

    this.admin = admin;
    this.request = request;
  }

  public Admin getAdmin() {
    return admin;
  }

  public Request getRequest() {
    return request;
  }

}
