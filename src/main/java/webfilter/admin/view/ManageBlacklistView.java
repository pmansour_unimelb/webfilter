package webfilter.admin.view;

import io.dropwizard.views.View;

import java.util.List;

import webfilter.admin.model.Admin;
import webfilter.proxy.model.BlockedWebsite;

/**
 * Represents the view of the admin panel to manage the blacklist.
 * 
 * @author peter
 */
public class ManageBlacklistView extends View {

  private static final String TEMPLATE_NAME = "ManageBlacklist.mustache";

  private Admin admin;
  private List<BlockedWebsite> blacklist;

  public ManageBlacklistView(Admin admin, List<BlockedWebsite> blacklist) {
    super(TEMPLATE_NAME);

    this.admin = admin;
    this.blacklist = blacklist;
  }

  public Admin getAdmin() {
    return admin;
  }

  public List<BlockedWebsite> getBlacklist() {
    return blacklist;
  }

}
