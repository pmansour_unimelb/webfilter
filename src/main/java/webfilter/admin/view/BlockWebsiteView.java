package webfilter.admin.view;

import io.dropwizard.views.View;
import webfilter.admin.model.Admin;

/**
 * View for an admin to add a website to the system blacklist.
 * 
 * @author peter
 */
public class BlockWebsiteView extends View {

  private static final String TEMPLATE_NAME = "BlockWebsite.mustache";

  private Admin admin;

  public BlockWebsiteView(Admin admin) {
    super(TEMPLATE_NAME);

    this.admin = admin;
  }

  public Admin getAdmin() {
    return admin;
  }

}
