package webfilter.application;

import io.dropwizard.Application;
import io.dropwizard.auth.basic.BasicAuthProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

import java.io.File;
import java.util.EnumSet;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.servlet.DispatcherType;

import org.eclipse.jetty.proxy.ProxyServlet;

import webfilter.admin.model.Admin;
import webfilter.admin.model.Request;
import webfilter.application.health.HelloHealthCheck;
import webfilter.proxy.model.BlockedWebsite;
import webfilter.proxy.model.Exemption;
import webfilter.proxy.model.WebsiteAccess;

import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricFilter;

/**
 * The main class that puts everything together for the web application.
 */
public class WebFilterApplication extends Application<WebFilterConfiguration> {

  // The realm to use for HTTP authentication.
  public static final String AUTH_REALM = "default";

  public static final String METRICS_DIRECTORY = "metrics/";

  private String systemName;

  // Create a hibernate bundle for the model classes.
  private final HibernateBundle<WebFilterConfiguration> hibernate =
      new HibernateBundle<WebFilterConfiguration>(BlockedWebsite.class, Exemption.class,
          WebsiteAccess.class, Admin.class, Request.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(WebFilterConfiguration configuration) {
          return configuration.getDataSourceFactory();
        }
      };

  @Override
  public void initialize(Bootstrap<WebFilterConfiguration> bootstrap) {
    bootstrap.addBundle(hibernate);
    bootstrap.addBundle(new ViewBundle());
  }

  @Override
  public void run(WebFilterConfiguration config, Environment environment) throws Exception {
    // Add a useless health check. Can remove this later if needed.
    environment.healthChecks().register("Hello check", new HelloHealthCheck("hello"));

    // Build the DAO, Service, and Controller factories.
    final DaoFactory daoFactory = new DaoFactory(hibernate.getSessionFactory());
    final ServiceFactory serviceFactory =
        new ServiceFactory(hibernate.getSessionFactory(), daoFactory);
    final ControllerFactory controllerFactory =
        new ControllerFactory(config, environment, serviceFactory);

    // Register the proxy-related controllers.
    environment.servlets().addServlet("Proxy Servlet", ProxyServlet.class).addMapping("/");
    environment.servlets().addFilter("Proxy Filter", controllerFactory.getProxyFilter())
        .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");

    // Register the admin-related controllers.
    environment.jersey().setUrlPattern("/adminPanel/*");
    environment.jersey().register(
        new BasicAuthProvider<Admin>(serviceFactory.getAuthenticatorService(), AUTH_REALM));

    environment.jersey().register(controllerFactory.getAccessAttemptsResource());
    environment.jersey().register(controllerFactory.getAdminPanelResource());
    environment.jersey().register(controllerFactory.getBlockWebsiteResource());
    environment.jersey().register(controllerFactory.getManageBlacklistResource());
    environment.jersey().register(controllerFactory.getManageRequestResource());
    environment.jersey().register(controllerFactory.getRequestCreationResource());
    environment.jersey().register(controllerFactory.getReviewRequestResource());
    environment.jersey().register(controllerFactory.getUnblockWebsiteResource());

    // Add some dummy data to the DB, so we can play around with it.
    serviceFactory.getDummyAdminDataCreatorService().createDummyData();
    serviceFactory.getDummyProxyDataCreatorService().createDummyData();

    // Start logging the Proxy's performance.
    final CsvReporter reporter =
        CsvReporter.forRegistry(environment.metrics()).formatFor(Locale.getDefault())
            // Only log the Proxy response time.
            .filter(new MetricFilter() {
              @Override
              public boolean matches(String name, Metric metric) {
                return name.equals("webfilter.proxy.controller.ProxyFilter.response.time");
              }
            }).convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS)
            .build(new File(METRICS_DIRECTORY));
    reporter.start(15, TimeUnit.SECONDS);
  }

  @Override
  public String getName() {
    return systemName;
  }

  public static void main(String[] args) throws Exception {
    new WebFilterApplication().run(args);
  }

}
