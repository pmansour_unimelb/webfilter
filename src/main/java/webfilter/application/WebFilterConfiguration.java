package webfilter.application;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The configuration for this Web Filter system. Loaded from a YAML file.
 */
public class WebFilterConfiguration extends Configuration {

  @NotEmpty
  private String systemName;
  @NotEmpty
  private String adminPanelUrl;

  // The database settings, to set up Hibernate.
  @Valid
  @NotNull
  @JsonProperty("database")
  private DataSourceFactory database = new DataSourceFactory();

  @JsonProperty
  public String getSystemName() {
    return systemName;
  }

  @JsonProperty
  public void setSystemName(String systemName) {
    this.systemName = systemName;
  }

  @JsonProperty
  public String getAdminPanelUrl() {
    return adminPanelUrl;
  }

  @JsonProperty
  public void setAdminPanelUrl(String adminPanelUrl) {
    this.adminPanelUrl = adminPanelUrl;
  }

  public DataSourceFactory getDataSourceFactory() {
    return database;
  }
}
