package webfilter.application;

import org.hibernate.SessionFactory;

import webfilter.admin.service.AccessAttemptGetterService;
import webfilter.admin.service.AuthenticatorService;
import webfilter.admin.service.BlacklistGetterService;
import webfilter.admin.service.DummyAdminDataCreatorService;
import webfilter.admin.service.RequestMakerService;
import webfilter.admin.service.RequestResolverService;
import webfilter.admin.service.UnresolvedRequestGetterService;
import webfilter.admin.service.WebsiteBlockerService;
import webfilter.admin.service.WebsiteUnblockerService;
import webfilter.proxy.service.DummyProxyDataCreatorService;
import webfilter.proxy.service.FilterService;

/**
 * A factory that builds and holds all of the created Services for use by Controller factories.
 * 
 * @author peter
 */
public class ServiceFactory {

  private AccessAttemptGetterService accessAttemptGetterService;
  private AuthenticatorService authenticatorService;
  private BlacklistGetterService blacklistGetterService;
  private DummyAdminDataCreatorService dummyAdminDataCreatorService;
  private DummyProxyDataCreatorService dummyProxyDataCreatorService;
  private FilterService filterService;
  private RequestMakerService requestMakerService;
  private RequestResolverService requestResolverService;
  private UnresolvedRequestGetterService unresolvedRequestGetterService;
  private WebsiteBlockerService websiteBlockerService;
  private WebsiteUnblockerService websiteUnblockerService;

  /**
   * Build all of the services using the DAOs from the given {@link DaoFactory}.
   * 
   * @param daoFactory The {@link DaoFactory} to use for building the services.
   */
  public ServiceFactory(SessionFactory sessionFactory, DaoFactory daoFactory) {
    this.accessAttemptGetterService =
        new AccessAttemptGetterService(daoFactory.getWebsiteAccessDao());
    this.authenticatorService = new AuthenticatorService(daoFactory.getAdminDao());
    this.blacklistGetterService = new BlacklistGetterService(daoFactory.getBlockedWebsiteDao());
    this.dummyAdminDataCreatorService =
        new DummyAdminDataCreatorService(daoFactory.getAdminDao(), daoFactory.getRequestDao());
    this.dummyProxyDataCreatorService =
        new DummyProxyDataCreatorService(daoFactory.getBlockedWebsiteDao(),
            daoFactory.getExemptionDao(), daoFactory.getWebsiteAccessDao());
    this.filterService =
        new FilterService(sessionFactory, daoFactory.getBlockedWebsiteDao(),
            daoFactory.getExemptionDao(), daoFactory.getWebsiteAccessDao());
    this.requestMakerService = new RequestMakerService(daoFactory.getRequestDao());
    this.websiteBlockerService = new WebsiteBlockerService(daoFactory.getBlockedWebsiteDao());
    this.websiteUnblockerService =
        new WebsiteUnblockerService(sessionFactory, daoFactory.getBlockedWebsiteDao(),
            daoFactory.getExemptionDao());

    // This service needs one of the previously defined services.
    this.requestResolverService =
        new RequestResolverService(sessionFactory, daoFactory.getBlockedWebsiteDao(),
            daoFactory.getExemptionDao(), daoFactory.getRequestDao(), websiteUnblockerService);
  }

  public AccessAttemptGetterService getAccessAttemptGetterService() {
    return accessAttemptGetterService;
  }

  public AuthenticatorService getAuthenticatorService() {
    return authenticatorService;
  }

  public BlacklistGetterService getBlacklistGetterService() {
    return blacklistGetterService;
  }

  public DummyAdminDataCreatorService getDummyAdminDataCreatorService() {
    return dummyAdminDataCreatorService;
  }

  public DummyProxyDataCreatorService getDummyProxyDataCreatorService() {
    return dummyProxyDataCreatorService;
  }

  public FilterService getFilterService() {
    return filterService;
  }

  public RequestMakerService getRequestMakerService() {
    return requestMakerService;
  }

  public RequestResolverService getRequestResolverService() {
    return requestResolverService;
  }

  public UnresolvedRequestGetterService getUnresolvedRequestGetterService() {
    return unresolvedRequestGetterService;
  }

  public WebsiteBlockerService getWebsiteBlockerService() {
    return websiteBlockerService;
  }

  public WebsiteUnblockerService getWebsiteUnblockerService() {
    return websiteUnblockerService;
  }

}
