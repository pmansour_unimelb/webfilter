package webfilter.application;

import org.hibernate.SessionFactory;

import webfilter.admin.dao.AdminDAO;
import webfilter.admin.dao.RequestDAO;
import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.dao.ExemptionDao;
import webfilter.proxy.dao.WebsiteAccessDao;

/**
 * A factory that builds and holds all of the created DAOs for use by service factories.
 * 
 * @author peter
 */
public class DaoFactory {

  private AdminDAO adminDao;
  private RequestDAO requestDao;
  private BlockedWebsiteDao blockedWebsiteDao;
  private ExemptionDao exemptionDao;
  private WebsiteAccessDao websiteAccessDao;


  /**
   * Build all of the DAOs with the given {@link SessionFactory}.
   * 
   * @param sessionFactory The {@link SessionFactory} to use when creating the DAOs.
   */
  public DaoFactory(SessionFactory sessionFactory) {
    this.adminDao = new AdminDAO(sessionFactory);
    this.requestDao = new RequestDAO(sessionFactory);
    this.blockedWebsiteDao = new BlockedWebsiteDao(sessionFactory);
    this.exemptionDao = new ExemptionDao(sessionFactory);
    this.websiteAccessDao = new WebsiteAccessDao(sessionFactory);
  }

  public AdminDAO getAdminDao() {
    return adminDao;
  }

  public RequestDAO getRequestDao() {
    return requestDao;
  }

  public BlockedWebsiteDao getBlockedWebsiteDao() {
    return blockedWebsiteDao;
  }

  public ExemptionDao getExemptionDao() {
    return exemptionDao;
  }

  public WebsiteAccessDao getWebsiteAccessDao() {
    return websiteAccessDao;
  }

}
