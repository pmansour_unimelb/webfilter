package webfilter.application;

import io.dropwizard.setup.Environment;
import webfilter.admin.controller.AccessAttemptsResource;
import webfilter.admin.controller.AdminPanelResource;
import webfilter.admin.controller.BlockWebsiteResource;
import webfilter.admin.controller.CreateRequestResource;
import webfilter.admin.controller.ManageBlacklistResource;
import webfilter.admin.controller.ManageRequestResource;
import webfilter.admin.controller.ReviewRequestResource;
import webfilter.admin.controller.UnblockWebsiteResource;
import webfilter.proxy.controller.ProxyFilter;

import com.codahale.metrics.Timer;

/**
 * A factory that builds and holds all of the created Controllers for registration with Jersey.
 * 
 * @author peter
 */
public class ControllerFactory {

  private AccessAttemptsResource accessAttemptsResource;
  private AdminPanelResource adminPanelResource;
  private BlockWebsiteResource blockWebsiteResource;
  private CreateRequestResource requestCreationResource;
  private ManageBlacklistResource manageBlacklistResource;
  private ManageRequestResource manageRequestResource;
  private ProxyFilter proxyFilter;
  private ReviewRequestResource reviewRequestResource;
  private UnblockWebsiteResource unblockWebsiteResource;

  /**
   * Build all of the controllers using the Services from the given {@link ServiceFactory}.
   * 
   * @param serviceFactory The {@link ServiceFactory} to use for building the controllers.
   */
  public ControllerFactory(WebFilterConfiguration config, Environment environment,
      ServiceFactory serviceFactory) {
    // Admin-related Controllers.
    this.accessAttemptsResource =
        new AccessAttemptsResource(serviceFactory.getAccessAttemptGetterService());
    this.adminPanelResource = new AdminPanelResource();
    this.blockWebsiteResource = new BlockWebsiteResource(serviceFactory.getWebsiteBlockerService());
    this.manageBlacklistResource =
        new ManageBlacklistResource(serviceFactory.getBlacklistGetterService());
    this.manageRequestResource =
        new ManageRequestResource(serviceFactory.getRequestResolverService());
    this.requestCreationResource =
        new CreateRequestResource(serviceFactory.getRequestMakerService());
    this.reviewRequestResource =
        new ReviewRequestResource(serviceFactory.getRequestResolverService());
    this.unblockWebsiteResource =
        new UnblockWebsiteResource(serviceFactory.getWebsiteUnblockerService());

    // Proxy-related Controllers.

    // Monitor the proxy's performance.
    final Timer proxyTimer =
        environment.metrics().timer("webfilter.proxy.controller.ProxyFilter.response.time");
    this.proxyFilter =
        new ProxyFilter(config.getAdminPanelUrl(), serviceFactory.getFilterService(), proxyTimer);
  }

  public AccessAttemptsResource getAccessAttemptsResource() {
    return accessAttemptsResource;
  }

  public AdminPanelResource getAdminPanelResource() {
    return adminPanelResource;
  }

  public BlockWebsiteResource getBlockWebsiteResource() {
    return blockWebsiteResource;
  }

  public ManageBlacklistResource getManageBlacklistResource() {
    return manageBlacklistResource;
  }

  public ManageRequestResource getManageRequestResource() {
    return manageRequestResource;
  }

  public ProxyFilter getProxyFilter() {
    return proxyFilter;
  }

  public CreateRequestResource getRequestCreationResource() {
    return requestCreationResource;
  }

  public ReviewRequestResource getReviewRequestResource() {
    return reviewRequestResource;
  }

  public UnblockWebsiteResource getUnblockWebsiteResource() {
    return unblockWebsiteResource;
  }

}
