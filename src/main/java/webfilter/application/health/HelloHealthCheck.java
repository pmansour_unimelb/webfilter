package webfilter.application.health;

import com.codahale.metrics.health.HealthCheck;

/**
 * A useless health check just to see how they work.
 */
public class HelloHealthCheck extends HealthCheck {

  private String hello;

  public HelloHealthCheck(String hello) {
    this.hello = hello;
  }

  @Override
  protected Result check() throws Exception {
    if (hello.equals("hello")) {
      return Result.healthy();
    } else {
      return Result.unhealthy("Hello variable does not equal hello!");
    }
  }

}
