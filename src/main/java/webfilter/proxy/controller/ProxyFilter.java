package webfilter.proxy.controller;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.http.HttpStatus;

import webfilter.proxy.service.FilterService;
import webfilter.proxy.service.WebsiteStatus;

import com.codahale.metrics.Timer;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

/**
 * Implements a web proxy filter while delegating the actual filtering logic to a given
 * {@link FilterService}.
 */
public class ProxyFilter implements javax.servlet.Filter {

  private static final String PAGE_FORBIDDEN_TEMPLATE =
      "webfilter/proxy/view/PageForbidden.mustache";

  private String adminPanelUrl;
  private FilterService filter;
  private Timer proxyTimer;

  /**
   * Create a new proxy filter using the given {@link FilterService} for the actual logic.
   * 
   * @param filter A {@link FilterService} to validate requests.
   */
  public ProxyFilter(String adminPanelUrl, FilterService filter, Timer proxyTimer) {
    this.adminPanelUrl = adminPanelUrl;
    this.filter = filter;
    this.proxyTimer = proxyTimer;
  }

  @Override
  public void destroy() {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    // Start timing this request.
    final Timer.Context context = proxyTimer.time();
    try {
      // We only filter HTTP(s) requests.
      if (request instanceof HttpServletRequest) {
        // Get the information needed to determine whether or not to proceed.
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final String userIp = httpRequest.getRemoteAddr();
        final String url = httpRequest.getRequestURL().toString();

        // Check whether or not the user is allowed to access it.
        final WebsiteStatus status = filter.checkStatus(userIp, url);
        if (status.isAllowed()) {
          chain.doFilter(request, response);
        } else {
          // Send a failure response if the user is not allowed access.
          handleForbiddenAccess(httpRequest, (HttpServletResponse) response, status);
        }
      }
    } finally {
      context.stop();
    }
  }

  @Override
  public void init(FilterConfig config) throws ServletException {}

  /**
   * Send a failed HTTP response back for blocked requests.
   * 
   * @param request The {@link HttpServletRequest} made by the client.
   * @param response The {@link HttpServletResponse} to write back to.
   * @throws IOException
   */
  private void handleForbiddenAccess(HttpServletRequest request, HttpServletResponse response,
      WebsiteStatus status) throws IOException {
    response.setStatus(HttpStatus.FORBIDDEN_403);

    // Set the admin panel URL in the object given to the template.
    status.setAdminPanelUrl(adminPanelUrl);

    final MustacheFactory mf = new DefaultMustacheFactory();
    Mustache mustache = mf.compile(PAGE_FORBIDDEN_TEMPLATE);
    mustache.execute(response.getWriter(), status);
  }
}
