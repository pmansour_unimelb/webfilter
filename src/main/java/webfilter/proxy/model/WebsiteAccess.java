package webfilter.proxy.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.google.common.base.Objects;

/**
 * Represents an attempted access (whether or not it's blocked)to a website by a user of the system.
 */
@Entity
@Table(name = "WebsiteAccess", uniqueConstraints = @UniqueConstraint(columnNames = {"userIp",
    "url", "timestamp"}))
public class WebsiteAccess {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private long id;
  @Column(name = "userIp", nullable = false)
  private String userIp;
  @Column(name = "url", nullable = false)
  private String url;
  @Column(name = "timestamp", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Calendar timestamp;
  @Column(name = "blocked", nullable = false)
  private Boolean blocked;

  /**
   * Empty constructor, to qualify as a JavaBean.
   */
  public WebsiteAccess() {}

  /**
   * Shorthand constructor, for initialising the fields.
   * 
   * @param userIp The IP address of the user that attempted the access.
   * @param url The URL that was being attempted.
   * @param timestamp The time at which the attempt was made.
   */
  public WebsiteAccess(String userIp, String url, Calendar timestamp, Boolean blocked) {
    this.userIp = userIp;
    this.url = url;
    this.timestamp = timestamp;
    this.blocked = blocked;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUserIp() {
    return userIp;
  }

  public void setUserIp(String userIp) {
    this.userIp = userIp;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Calendar getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Calendar timestamp) {
    this.timestamp = timestamp;
  }

  public Boolean isBlocked() {
    return blocked;
  }

  public void setBlocked(Boolean blocked) {
    this.blocked = blocked;
  }

  /**
   * Get a string representation of the date at which this access was attempted, in the format
   * dd/mm/yyyy.
   */
  public String getDateString() {
    return new SimpleDateFormat("dd/MM/yyyy").format(timestamp.getTime());
  }

  /**
   * Get a string representation of the time at which this access was attempted, in the format
   * hh:mm:ssAM/PM.
   */
  public String getTimeString() {
    return new SimpleDateFormat("KK:mm:ssa").format(timestamp.getTime());
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("id", id).add("userIp", userIp).add("url", url)
        .add("isBlocked", blocked).add("timestamp", timestamp.toString())
        .add("timeString", getTimeString()).add("dateString", getDateString()).toString();
  }
}
