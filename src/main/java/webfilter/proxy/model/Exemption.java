package webfilter.proxy.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.google.common.base.Objects;
import com.google.common.base.Optional;

/**
 * Represents an exemption for a particular user with a particular blocked website.
 */
@Entity
@Table(name = "Exemption", uniqueConstraints = @UniqueConstraint(columnNames = {"userIp",
    "hostname"}))
public class Exemption {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private long id;
  @Column(name = "userIp", nullable = false)
  private String userIp;
  @Column(name = "hostname", nullable = false)
  private String hostname;
  @Column(name = "reason", nullable = false)
  private String reason;
  @Column(name = "expiryDate", nullable = true)
  @Temporal(TemporalType.TIMESTAMP)
  private Calendar expiryDate;

  /**
   * Empty constructor, to qualify as a JavaBean.
   */
  public Exemption() {}

  /**
   * Shorthand constructor, for initialising the fields.
   * 
   * @param userIp The IP address of the user to which this exemption applies.
   * @param hostname The host name of the website to which this exemption applies.
   * @param reason The reason for this exemption.
   * @param expiryDate The date at which this exemption expires. Optional. If not given, then this
   *        exemption will never expire.
   */
  public Exemption(String userIp, String hostname, String reason, Optional<Calendar> expiryDate) {
    this.userIp = userIp;
    this.hostname = hostname;
    this.reason = reason;
    this.expiryDate = expiryDate.orNull();
  }

  public Optional<Calendar> getExpiryDate() {
    return Optional.fromNullable(expiryDate);
  }

  public void setExpiryDate(Calendar expiryDate) {
    this.expiryDate = expiryDate;
  }

  public String getHostname() {
    return hostname;
  }

  public void setHostname(String hostname) {
    this.hostname = hostname;
  }

  public String getUserIp() {
    return userIp;
  }

  public void setUserIp(String userIp) {
    this.userIp = userIp;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("id", id).add("hostname", hostname)
        .add("userIp", userIp).add("expiryDate", expiryDate).add("reason", reason).toString();
  }
}
