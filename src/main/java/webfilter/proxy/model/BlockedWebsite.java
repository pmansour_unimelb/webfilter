package webfilter.proxy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.common.base.Objects;

/**
 * Represents a website in the system blacklist.
 */
@Entity
@Table(name = "BlockedWebsite")
public class BlockedWebsite {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private long id;
  @Column(name = "hostname", unique = true, nullable = false)
  private String hostname;
  @Column(name = "reason", nullable = false)
  private String reason;

  /**
   * Empty constructor, to qualify as a JavaBean.
   */
  public BlockedWebsite() {}

  /**
   * Shorthand constructor, for initialising the fields.
   * 
   * @param hostname The blocked website hostname.
   * @param reason The reason for the block.
   */
  public BlockedWebsite(String hostname, String reason) {
    this.hostname = hostname;
    this.reason = reason;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getHostname() {
    return hostname;
  }

  public void setHostname(String hostname) {
    this.hostname = hostname;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("id", id).add("hostname", hostname)
        .add("reason", reason).toString();
  }
}
