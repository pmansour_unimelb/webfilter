package webfilter.proxy.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.dao.ExemptionDao;
import webfilter.proxy.dao.WebsiteAccessDao;
import webfilter.proxy.model.BlockedWebsite;
import webfilter.proxy.model.Exemption;
import webfilter.proxy.model.WebsiteAccess;

import com.google.common.base.Optional;

/**
 * Encapsulates the filtering logic for the system. Uses DAOs to access and/or modify the DB.
 */
public class FilterService {

  // Shown to the user if the URL being parsed is somehow malformed.
  private static final String MALFORMED_URL_ERROR = "Internal Error: Malformed URL.";

  private SessionFactory sessionFactory;
  private BlockedWebsiteDao blockedWebsiteDao;
  private ExemptionDao exemptionDao;
  private WebsiteAccessDao websiteAccessDao;

  /**
   * Constructor for a Filter that gets some DAOs injected in.
   */
  public FilterService(SessionFactory sessionFactory, BlockedWebsiteDao blockedWebsiteDao,
      ExemptionDao exemptionDao, WebsiteAccessDao websiteAccessDao) {
    this.sessionFactory = sessionFactory;
    this.blockedWebsiteDao = blockedWebsiteDao;
    this.exemptionDao = exemptionDao;
    this.websiteAccessDao = websiteAccessDao;

  }

  /**
   * Determine whether or not the given user is allowed to access the given URL at this time.
   * 
   * @param userIp The IP address of the requesting user.
   * @param url The URL being requested.
   * @return A {@link WebsiteStatus} object encapsulating the results of the check.
   */
  public WebsiteStatus checkStatus(String userIp, String url) {
    final Session session;
    final Optional<BlockedWebsite> blockedWebsite;
    final Optional<Exemption> exemption;
    final String reason, hostname;
    final boolean isAllowed;

    // Perform this transaction in one session.
    session = sessionFactory.openSession();
    try {
      // Get the hostname out of the URL.
      try {
        hostname = new URL(url).getHost();
      } catch (MalformedURLException e1) {
        return new WebsiteStatus(userIp, url, MALFORMED_URL_ERROR, false);
      }

      // Look in the DB for website matches.
      blockedWebsite = blockedWebsiteDao.findByHostname(session, hostname);
      exemption = exemptionDao.findByUserIpAndHostname(session, userIp, hostname);

      // A user is allowed access if there's both a block and an exemption, or if there's neither.
      isAllowed = blockedWebsite.isPresent() == exemption.isPresent();

      // Record the users attempt regardless.
      websiteAccessDao.add(session, new WebsiteAccess(userIp, url, Calendar.getInstance(),
          !isAllowed));

      if (isAllowed) {
        return new WebsiteStatus(userIp, url);
      } else {
        // Find the actual reason for the block (either from the block, or from an exemption).
        reason =
            (blockedWebsite.isPresent()) ? blockedWebsite.get().getReason() : exemption.get()
                .getReason();
        return new WebsiteStatus(userIp, url, reason, true);
      }
    } finally {
      session.close();
    }
  }
}
