package webfilter.proxy.service;

import java.net.MalformedURLException;
import java.net.URL;

import com.google.common.base.Objects;

/**
 * The status of a website access attempt for a particular user and URL pair.
 */
public class WebsiteStatus {

  // Some stuff about the request.
  private String userIp;
  private String url;

  // Some stuff about how it went.
  private boolean allowed;
  private String reason;
  private boolean accessAttemptRecorded;

  private String adminPanelUrl;

  /**
   * Constructor for the case where the user is allowed access. Fills the remaining fields with
   * defaults for allowing access.
   * 
   * @param userIp The IP address of the user attempting the access.
   * @param url The URL being attempted.
   */
  public WebsiteStatus(String userIp, String url) {
    this.userIp = userIp;
    this.url = url;

    this.allowed = true;
    this.reason = null;
    this.accessAttemptRecorded = false;
  }

  /**
   * Constructor for the case where the user is not allowed access. Fills the remaining fields with
   * defaults for not allowing access.
   * 
   * @param userIp The IP address of the user attempting the access.
   * @param url The URL being attempted.
   * 
   * @param reason The reason for why the user is not allowed access.
   * @param accessAttemptRecorded Whether or not the users access attempt was recorded.
   */
  public WebsiteStatus(String userIp, String url, String reason, boolean accessAttemptRecorded) {
    this.userIp = userIp;
    this.url = url;

    this.allowed = false;
    this.reason = reason;
    this.accessAttemptRecorded = accessAttemptRecorded;
  }

  public String getUserIp() {
    return userIp;
  }

  public String getUrl() {
    return url;
  }

  public String getHostname() {
    // Lazily get the hostname out of the given URL.
    try {
      return new URL(url).getHost();
    } catch (MalformedURLException e) {
      e.printStackTrace(System.err);
      return "";
    }
  }

  public boolean isAllowed() {
    return allowed;
  }

  public String getReason() {
    return reason;
  }

  public boolean isAccessAttemptRecorded() {
    return accessAttemptRecorded;
  }

  public String getAdminPanelUrl() {
    return adminPanelUrl;
  }

  public void setAdminPanelUrl(String adminPanelUrl) {
    this.adminPanelUrl = adminPanelUrl;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("userIp", userIp).add("url", url)
        .add("isAllowed", allowed).add("reason", reason).add("adminPanelUrl", adminPanelUrl)
        .toString();
  }
}
