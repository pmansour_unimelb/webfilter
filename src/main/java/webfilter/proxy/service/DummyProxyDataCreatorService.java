package webfilter.proxy.service;

import java.util.Calendar;

import org.hibernate.HibernateException;

import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.dao.ExemptionDao;
import webfilter.proxy.dao.WebsiteAccessDao;
import webfilter.proxy.model.BlockedWebsite;
import webfilter.proxy.model.Exemption;
import webfilter.proxy.model.WebsiteAccess;

import com.google.common.base.Optional;

/**
 * Simple service to create some dummy data for testing the proxy.
 * 
 * @author peter
 */
public class DummyProxyDataCreatorService {

  private BlockedWebsiteDao blockedWebsiteDao;
  private ExemptionDao exemptionDao;
  private WebsiteAccessDao websiteAccessDao;

  /**
   * Build the service using the given DAOs.
   * 
   * @param blockedWebsiteDao The DAO to use for persisting {@link BlockedWebsite} objects.
   * @param exemptionDao The DAO to use for persisting {@link Exemption} objects.
   * @param websiteAccessDao The DAO to use for persisting {@link WebsiteAccess} objects.
   */
  public DummyProxyDataCreatorService(BlockedWebsiteDao blockedWebsiteDao,
      ExemptionDao exemptionDao, WebsiteAccessDao websiteAccessDao) {
    this.blockedWebsiteDao = blockedWebsiteDao;
    this.exemptionDao = exemptionDao;
    this.websiteAccessDao = websiteAccessDao;
  }

  /**
   * Create some dummy data to go into the DB.
   * 
   * @throws HibernateException
   */
  public void createDummyData() throws HibernateException {
    Calendar tomorrow = Calendar.getInstance();
    tomorrow.add(Calendar.DAY_OF_MONTH, 1);

    blockedWebsiteDao.add(new BlockedWebsite("facebook.com", "Wastes time."));
    exemptionDao.add(new Exemption("0.0.0.0", "facebook.com",
        "Because that IP doesn't really exist.", Optional.of(tomorrow)));
    websiteAccessDao.add(new WebsiteAccess("127.0.0.1", "http://lms.unimelb.edu.au", Calendar
        .getInstance(), false));
  }

}
