package webfilter.proxy.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import webfilter.proxy.model.WebsiteAccess;

/**
 * Controls the persistence of {@link WebsiteAccess} objects.
 */
public class WebsiteAccessDao extends AbstractDAO<WebsiteAccess> {

  // Queries for use by the DAO.
  private static final String Q_FIND_ALL_WEBSITE_ACCESSES = "from WebsiteAccess wa";

  private SessionFactory factory;

  public WebsiteAccessDao(SessionFactory factory) {
    super(factory);
    this.factory = factory;
  }

  public void add(WebsiteAccess access) throws HibernateException {
    Session session = factory.openSession();
    try {
      add(session, access);
    } finally {
      session.close();
    }
  }

  /**
   * Persist a new {@link WebsiteAccess} object. Assumes a new object, as it will be given a new
   * identifier.
   * 
   * @param session The DB session to use for this operation.
   * @param access The new {@link WebsiteAccess} to be persisted.
   * @throws HibernateException
   */
  public void add(Session session, WebsiteAccess access) throws HibernateException {
    session.save(access);
    session.flush();
  }

  /**
   * Find all the {@link WebsiteAccess}s recorded in the DB.
   * 
   * @return All the {@link WebsiteAccess}s in the DB.
   * @throws HibernateException
   */
  public List<WebsiteAccess> findAll() throws HibernateException {
    Session session = factory.openSession();
    try {
      return list(session.createQuery(Q_FIND_ALL_WEBSITE_ACCESSES));
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Delete the given {@link WebsiteAccess} object from the database.
   * 
   * @param access The {@link WebsiteAccess} to delete. Must already exist in the DB, and hence must
   *        have a valid ID.
   * @throws HibernateException
   */
  public void delete(WebsiteAccess access) throws HibernateException {
    Session session = factory.openSession();
    try {
      session.delete(access);
      session.flush();
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

}
