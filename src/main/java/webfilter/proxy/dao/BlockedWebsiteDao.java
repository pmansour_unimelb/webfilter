package webfilter.proxy.dao;

import io.dropwizard.hibernate.AbstractDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import webfilter.proxy.model.BlockedWebsite;

import com.google.common.base.Optional;

/**
 * Controls the persistence of {@link BlockedWebsite} objects.
 */
public class BlockedWebsiteDao extends AbstractDAO<BlockedWebsite> {

  // Queries to be used by the DAO.
  private static final String Q_FIND_ALL_BLOCKED_WEBSITES = "from BlockedWebsite bw";
  private static final String Q_FIND_BLOCKED_WEBSITE_BY_HOST_NAME =
      "from BlockedWebsite bw where bw.hostname = :hostname";
  private static final String Q_FLD_HOSTNAME = "hostname";


  private SessionFactory factory;

  public BlockedWebsiteDao(SessionFactory factory) {
    super(factory);
    this.factory = factory;
  }

  /**
   * Persist the given {@link BlockedWebsite} object. Assumes a new object, as it will be given a
   * new identifier.
   * 
   * @param website The new {@link BlockedWebsite} to be persisted.
   * @return The newly-persisted website's ID, or Optional.absent().
   * @throws HibernateException
   */
  public Optional<Long> add(BlockedWebsite website) throws HibernateException {
    // Add the object using a new session.
    Session session = factory.openSession();
    try {
      session.save(website);
      session.flush();

      return Optional.fromNullable(website.getId());
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Get the single {@link BlockedWebsite} corresponding to the given ID.
   * 
   * @param id The ID of the {@link BlockedWebsite} to get.
   * @param session An open session in the middle of a transaction. This method does not flush or
   *        close the given session.
   * @return A {@link BlockedWebsite} or Optional.Absent().
   * @throws HibernateException
   */
  public Optional<BlockedWebsite> getById(Long id, Session session) throws HibernateException {
    try {
      return Optional.fromNullable((BlockedWebsite) session.get(BlockedWebsite.class, id));
    } catch (HibernateException e) {
      throw e;
    }
  }

  /**
   * Find all the {@link BlockedWebsite}s in the database that match the given hostname.
   * 
   * @param hostname The host name to check against.
   * @return A matching {@link BlockedWebsite}, or Optional.absent().
   * @throws HibernateException
   */
  public Optional<BlockedWebsite> findByHostname(String hostname) throws HibernateException {
    // Execute the query and return a single result, in a new DB session.
    Session session = factory.openSession();
    try {
      return findByHostname(session, hostname);
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Find all the {@link BlockedWebsite}s in the database that match the given hostname within a
   * given session.
   * 
   * @param session The session to use for this query.
   * @param hostname The host name to check against.
   * @return A matching {@link BlockedWebsite}, or Optional.absent().
   * @throws HibernateException
   */
  public Optional<BlockedWebsite> findByHostname(Session session, String hostname)
      throws HibernateException {
    return Optional.fromNullable(uniqueResult(session.createQuery(
        Q_FIND_BLOCKED_WEBSITE_BY_HOST_NAME).setString(Q_FLD_HOSTNAME, hostname)));
  }

  /**
   * Find all the {@link BlockedWebsite}s in the DB.
   * 
   * @return A list of all the {@link BlockedWebsite}s in the DB.
   * @throws HibernateException
   */
  public List<BlockedWebsite> findAll() throws HibernateException {
    Session session = factory.openSession();
    try {
      return list(session.createQuery(Q_FIND_ALL_BLOCKED_WEBSITES));
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Delete the given {@link BlockedWebsite} object from the database.
   * 
   * @param website The {@link BlockedWebsite} to delete. Must already exist in the DB, and hence
   *        must have a valid ID.
   * @param session An open session in the middle of a transaction. This method does not flush or
   *        close the given session.
   * @throws HibernateException
   */
  public void delete(BlockedWebsite website, Session session) throws HibernateException {
    try {
      session.delete(website);
    } catch (HibernateException e) {
      throw e;
    }
  }

}
