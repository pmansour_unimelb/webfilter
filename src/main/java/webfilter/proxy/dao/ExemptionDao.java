package webfilter.proxy.dao;

import io.dropwizard.hibernate.AbstractDAO;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import webfilter.proxy.model.Exemption;

import com.google.common.base.Optional;

/**
 * Controls the persistence of {@link Exemption} objects.
 */
public class ExemptionDao extends AbstractDAO<Exemption> {

  // Queries to be used by the DAO.
  private static final String Q_FIND_EXEMPTION_BY_USERIP_AND_HOSTNAME =
      "from Exemption e where e.userIp = :userIp AND e.hostname = :hostname";
  private static final String Q_DELETE_BY_HOSTNAME =
      "delete Exemption e where e.hostname = :hostname";

  private static final String Q_FLD_USER_IP = "userIp";
  private static final String Q_FLD_HOSTNAME = "hostname";

  private SessionFactory factory;

  public ExemptionDao(SessionFactory factory) {
    super(factory);
    this.factory = factory;
  }

  /**
   * Persist the given {@link Exemption} object. Assumes a new object, as it will be given a new
   * identifier.
   * 
   * @param exemption The new {@link Exemption} to be persisted.
   * @throws HibernateException
   */
  public void add(Exemption exemption) throws HibernateException {
    Session session = factory.openSession();
    try {
      add(session, exemption);
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

  /**
   * Persist the given {@link Exemption} object. Assumes a new object, as it will be given a new
   * identifier.
   * 
   * @param session The session to use for this save.
   * @param exemption The new {@link Exemption} to be persisted.
   * @throws HibernateException
   */
  public void add(Session session, Exemption exemption) throws HibernateException {
    session.save(exemption);
    session.flush();
  }

  /**
   * Find all the {@link Exemption}s in the database that match the given user and hostname.
   * 
   * @param session The DB session to use for this query.
   * @param userIp The IP address of the user to check against.
   * @param hostname The host name to check against.
   * @return A matching {@link Exemption}, or an absent value.
   * @throws HibernateException
   */
  public Optional<Exemption> findByUserIpAndHostname(Session session, String userIp, String hostname)
      throws HibernateException {
    return Optional.fromNullable(uniqueResult(session
        .createQuery(Q_FIND_EXEMPTION_BY_USERIP_AND_HOSTNAME).setString(Q_FLD_HOSTNAME, hostname)
        .setString(Q_FLD_USER_IP, userIp)));
  }

  /**
   * Delete all the {@link Exemption}s in the system with the given hostname, in the given session.
   * 
   * @param hostname The hostname to find and delete {@link Exemption}s by.
   * @param session The open session to use for the delete. Assumed to be open. This method won't
   *        close it or end a transaction.
   * @return The number of {@link Exemption}s actually deleted.
   * @throws HibernateException
   */
  public Integer deleteAllByHostname(String hostname, Session session) throws HibernateException {
    try {
      return session.createQuery(Q_DELETE_BY_HOSTNAME).setString(Q_FLD_HOSTNAME, hostname)
          .executeUpdate();
    } catch (HibernateException e) {
      throw e;
    }
  }

  /**
   * Delete the given {@link Exemption} object from the database.
   * 
   * @param exemption The {@link Exemption} to delete. Must already exist in the DB, and hence must
   *        have a valid ID.
   * @throws HibernateException
   */
  public void delete(Exemption exemption) throws HibernateException {
    Session session = factory.openSession();
    try {
      session.delete(exemption);
      session.flush();
    } catch (HibernateException e) {
      throw e;
    } finally {
      session.close();
    }
  }

}
