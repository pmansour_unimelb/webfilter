package webfilter.bla;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/adminPanel")
@Produces(MediaType.TEXT_PLAIN)
public class MyFavouriteResource {

  public MyFavouriteResource() {

  }

  @GET
  public String getMe() {
    return "Hello! Welcome to my favourite resource.";
  }
}
