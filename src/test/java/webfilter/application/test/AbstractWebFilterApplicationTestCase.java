package webfilter.application.test;

import static org.mockito.Mockito.mock;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Environment;
import junit.framework.TestCase;
import webfilter.application.WebFilterApplication;
import webfilter.application.WebFilterConfiguration;

/**
 * Subclass this test case to test DropWizard application related classes.
 */
public abstract class AbstractWebFilterApplicationTestCase extends TestCase {

  protected Environment environment;
  protected JerseyEnvironment jersey;
  protected WebFilterApplication application;
  protected WebFilterConfiguration config;

  @Override
  public void setUp() throws Exception {
    super.setUp();

    environment = mock(Environment.class);
    jersey = mock(JerseyEnvironment.class);
    application = new WebFilterApplication();
    config = new WebFilterConfiguration();
  }

  @Override
  public void tearDown() throws Exception {
    super.tearDown();

    environment = null;
    jersey = null;
    application = null;
    config = null;
  }
}
