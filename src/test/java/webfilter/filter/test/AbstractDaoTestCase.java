package webfilter.filter.test;

import static org.mockito.Mockito.mock;
import junit.framework.TestCase;
import webfilter.proxy.dao.BlockedWebsiteDao;
import webfilter.proxy.dao.ExemptionDao;
import webfilter.proxy.dao.WebsiteAccessDao;

/**
 * Subclass this test case to test a class that needs one of the DAOs.
 */
public abstract class AbstractDaoTestCase extends TestCase {

  protected BlockedWebsiteDao blockedWebsiteDao;
  protected ExemptionDao exemptionDao;
  protected WebsiteAccessDao websiteAccessDao;

  @Override
  protected void setUp() throws Exception {
    super.setUp();

    blockedWebsiteDao = mock(BlockedWebsiteDao.class);
    exemptionDao = mock(ExemptionDao.class);
    websiteAccessDao = mock(WebsiteAccessDao.class);
  }

  @Override
  protected void tearDown() throws Exception {
    super.tearDown();

    blockedWebsiteDao = null;
    exemptionDao = null;
    websiteAccessDao = null;
  }

}
