/**
 * @file Contains a simple program to bombard a URL with lots of requests.
 * This program can take a URL, the number of requests to fire in one go, and
 * how often to repeat this bombardment (in ms), and infinitely repeat this.
 * The program will attempt to GET a random 5-character resource on the given
 * URL.
 *
 * Usage:
 *	npm install request
 *	node endurance.js {proxy} {url} {num-requests} {timeout}
 *
 * Where:
 *	{proxy}					is the proxy to make the request through. Defaults to
 *									'http://localhost:8080'.
 *	{url}				  	is the URL string to bombard. Will have 5-character
 *									resources	appended to it. Does not need a trailing slash.
 *									Defaults to 'http://facebook.com'.
 *	{num-requests}	is the number of requests to make at each round. Defaults
 *									to 10.
 *	{timeout}				is the amount of time (in ms) to wait between each round.
 *									Defaults to 1000 (1 second).
 *
 * @author pmansour.
 */

var request = require('request');

var defaultProxy = 'http://localhost:8080';
var defaultUrl = 'http://facebook.com';
var defaultNumRequests = 10;
var defaultTimeout = 1000;

var saltLength = 5;
var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.split('');
var maxCharIndex = chars.length - 1;

/**
 * Standard function to generate a random integer between min and max.
 */
var getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};

/**
 * Generate a random string of length *saltLength*, consisting of characters
 * in *chars*.
 */
var generateSaltResource = function() {
	var string = ['/'];
	for (var i = 0; i < saltLength; i++) {
		string.push(chars[getRandomInt(0, maxCharIndex)]);
	}
	return string.join('');
};

/**
 * The basic function used to shoot *numRequests* requests at the given URL
 * on a random resource.
 */
var createLotsOfRequests = function(proxy, baseUrl, numRequests) {
	for (var i = 0; i < numRequests; i++) {
		var requestUri = baseUrl + generateSaltResource();
		request({uri: requestUri, proxy: proxy}, function(err) {});
	}
};

/**
 * Start the bombardment!
 */
var start = function() {
	// Load the command-line arguments, or use defaults.
	var proxy = process.argv[2] || defaultProxy,
			url = process.argv[3] || defaultUrl,
			numRequests = parseInt(process.argv[4]) || defaultNumRequests,
			timeout = parseInt(process.argv[5]) || defaultTimeout;

	setInterval(createLotsOfRequests, timeout, proxy, url, numRequests);
};

start();
